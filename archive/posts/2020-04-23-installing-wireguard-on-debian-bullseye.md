---
title: "Installing Wireguard on Debian Bullseye"
date: 2020-04-23T13:34:46+02:00
---

## 2020-11-02 update

The issue is fixed, just use:
```bash
apt install wireguard
```

and your Debian Bullseye machine is ready for Wireguard!

<!--more-->

## The original article

Wireguard is a new VPN solution included right in the Linux kernel. It's part of the mainline kernel since [version 5.6](https://www.phoronix.com/scan.php?page=article&item=linux-56-features&num=1). Debian Bullseye has Linux 5.5, but the distribution maintainers decided to backport Wireguard, so it's already available by default. Sadly, this broke the installation process that is usually suggested on the Internet.

Now, the installation is even simpler, it's just needed to install the `wireguard-tools` package without the recommended dependencies:

```bash
apt install --no-install-recommends wireguard-tools
```

Without the `--no-install-recommends`, `apt` installs also the `wireguard-dkms` package. This package tries building the kernel module, but it fails because the code conflicts with some of the backported kernel features.
