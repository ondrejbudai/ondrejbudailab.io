---
title: "Downloading RPMs From Fedora Bodhi"
date: 2020-11-02T12:13:10+01:00
summary: |
    TL;DR: bodhi updates download --builds [NEVR]
---

Today, Christian Kellner showed me a cool trick for downloading RPMs from [Fedora Bodhi](https://bodhi.fedoraproject.org/).

You can use the `bodhi` command from the `bodhi-client` package to do that. The command is actually very simple:

```bash
bodhi updates download --builds osbuild-23-1.fc33
```

where `osbuild-23-1.fc33` is the build NEVR. This command downloads all RPMs associated with a Bodhi update that the specified build belongs to. For packages with arch-specific RPMs, it only downloads those ones for the architecture of the current host. Installing the downloaded packages is therefore very simple:

```bash
dnf install -y *.rpm
```

You may want to ask how this approach differs from `dnf install --enablerepo=updates-testing`. The nice thing about the longer `bodhi-client` method is that you can use it to install packages that are not yet available in the `updates-testing` repository. Pretty neat, right?
