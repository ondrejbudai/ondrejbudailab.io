---
title: "How to use the clipboard in a terminal"
date: 2020-04-17T11:35:45+02:00
summary: |
    Have you ever needed to paste your public SSH key into the browser?
    Here's a quick guide how to get it into the clipboard using terminal.
---

Sometimes it's handy to copy a command output or paste a piece of text into a unix pipe. Fortunately, there are handy commands to do it.
## Xorg

The interaction with the Xorg clipboard is done using the `xclip` command. The `xclip` command is in the `xclip` package in Fedora:

```bash
sudo dnf install xclip
```

To copy a command output to the clipboard, just use:

```bash
echo "I am now in your clipboard" | xclip
```

To get the clipboard content as a command output, the following command is useful:

```bash
xclip -o
```

Of course, this is much more useful with pipes. The following example pipes the clipboard content into the `less` pager:

```bash
xclip -o | less
```


## Wayland

If you use a more brave Linux distribution, you may use the Wayland project instead of Xorg. In this case, you can use the [`wl-clipboard` project](https://github.com/bugaevc/wl-clipboard). You can install it in Fedora from the `wl-clipboard` package:

```bash
sudo dnf install wl-clipboard
```

To copy a command output to the clipboard, you can use `wl-copy`:

```bash
echo "I am now in your wayland clipboard" | wl-copy
```

Pasting is done using the `wl-paste` command:

```bash
wl-paste | less
```

## Using these tools to get your public key to the clipboard

I often spin up virtual machines in a cloud. To be able to log in to them using my public SSH key, it's usually needed to write the key into some kind of web interface. To simplify that process I use this handy command:

```bash
# Xorg version
cat ~/.ssh/id_rsa.pub | xclip

# Wayland version
cat ~/.ssh/id_rsa.pub | wl-copy
```

This instantly puts my public key to the clipboard and I can just alt-tab to the browser and paste it to the right field. Simple, isn't it?
