---
title: "Booting Fedora Server from USB on Raspberry Pi 4"
date: 2021-03-07T13:05:34+01:00
summary: |
    I like to run Fedora on everything. The process of setting it up on raspi is unfortunately not that straightforward as it should be.
---

I've recently got a new Raspberry Pi 4 (the 8GB model). Of course, I wanted to run Fedora on it. First decision is which image I should use: Fedora provides both 32-bit `armhfp` and 64-bit `aarch64` images. As I wanted to use the full power of the board, I naturally went for the `aarch64` image. I followed [the official guide](https://fedoraproject.org/wiki/Architectures/ARM/Raspberry_Pi#Manual) and was able to quickly boot my Pi from an SD card ([Samsung Evo Plus 32GB](https://www.samsung.com/uk/memory-storage/memory-card/evo-plus-microsd-card-32gb-mb-mc32ga-eu/)).

I have no interest in a desktop environment so I used the Fedora Server image. I did some quick experiments in terminal (e.g. building a custom Fedora image with [osbuild-composer](https://github.com/osbuild/osbuild-composer)) but I was quickly disappointed with the performance. The Pi has 4 cores, 8 GB of RAM but an image build still takes half an hour.

Building an operating system is an IO-heavy task so I suspected that even though I have one of the fastest SD cards, it's still the limiting factor. Therefore, I decided to try booting Fedora from a USB drive.

## Preparing the Pi

I found one spare 256 GB NVMe M.2 SSD at home and bought a USB 3 to NVME adapter ([AXAGON EEM2-UG2](https://www.axagon.eu/en/produkty/eem2-ug2)). Then, I discovered this [wonderful guide on Tom's Hardware](https://www.tomshardware.com/how-to/boot-raspberry-pi-4-usb) describing how to enable USB boot with Raspberry Pi OS.

The first issue with the guide came soon: Fedora doesn't ship the `rpi-eeprom-update` and `raspi-config` tools. Well, the solution was pretty simple - I just put the official Raspberry Pi OS on the SD card, booted up from it and followed the steps from the guide.

I copied the contents of the SD card to the NVMe drive and voila - my Raspberry Pi was able to boot Raspberry Pi OS from USB.

## First attempt to boot Fedora from USB

My next step was to once more [write a Fedora Server image](https://fedoraproject.org/wiki/Architectures/ARM/Raspberry_Pi#Manual) but this time to the NVMe drive. When the copy operation was done, I plugged the drive into my Pi, connect the power cord and... nothing happened. I decided to continue this project with a rage quit.

## Here comes the UEFI firmware

After several weeks, I decided to give my Raspberry & Fedora setup another look. I found [this amazing guide](https://fwmotion.com/blog/operating-systems/2020-09-04-installing-fedora-server-onto-pi4/) that describes exactly what I need.

The guide does one part very differently from what I did in the previous attempt: Instead of using the official firmware and u-boot bootloader (I might be wrong with these terms, sorry!), it uses an unofficial [UEFI-compatible firmware](https://github.com/pftf/RPi4). This is awesome because the Fedora's `aarch64` image natively supports UEFI boot.

> Please note that the UEFI firmware doesn't support all Raspberry's features and it's very experimental. For example, it doesn't support Wi-Fi nor GPIO pins. As I would like to use my Raspberry as a headless server, this is fine for me.

One part of the guide sounded unnecessarily complex to me though. The guide uses two drives - one drive is written with the UEFI firmware and the other one is loaded with the Fedora installer. At this point, I asked myself: Isn't it possible to just use the Fedora Server image with the addition of the UEFI firmware to the EFI partition? I naturally tried it!

## Second (and successful) attempt to boot Fedora from USB

Firstly, I copied the Fedora Server `aarch64` image onto the SD card (it's connected as `/dev/sdb` on my laptop):

```bash
dd bs=4M if=./Fedora-Server-33-1.3.aarch64.raw of=/dev/sdb
```

Then, I mounted the first (EFI) partition:

```bash
mkdir /mnt/rpi
mount /dev/sdb1 /mnt/rpi
```

I downloaded [the newest UEFI firmware](https://github.com/pftf/RPi4/releases) and unzipped it into the EFI partition:

```bash
cd /mnt/rpi
unzip ~/RPi4_UEFI_Firmware_v1.24.zip
```

The last step was to safely unmount the partition:

```bash
cd -
sync
umount /mnt/rpi
rmdir /mnt/rpi
```

Then, I plugged the USB drive into my Raspberry, turned on the power - and it BOOTED! I tried updating the kernel and the Pi was still bootable afterward. I tried once more to make a Fedora image using `osbuild-composer` and this time it was done in 8 minutes! That's much less than the first attempt (30 minutes).

> Note that one extra step is highly recommended: By default, the UEFI firmware limits the RAM capacity to 3 GB because older kernels aren't able to boot with more memory. Fedora 33, fortunately, ships a new enough firmware so it's possible to enable all the available memory. See the [UEFI firmware's README](https://github.com/pftf/RPi4#initial-notice) for steps on how to achieve this.

## Summary

To be able to boot Fedora Server from USB you need to do two steps:

1) Update the raspi's configuration so it can boot from USB. You need to use Raspberry Pi OS for this step.
2) Use [my guide](#Second-and-successful-attempt-to-boot-Fedora-from-USB) to write Fedora Server image to the USB drive and add the UEFI bootloader to it.

> A word of warning: Using the unofficial UEFI firmware may damage your device. Be careful what you do. This article is provided without warranty of any kind.
