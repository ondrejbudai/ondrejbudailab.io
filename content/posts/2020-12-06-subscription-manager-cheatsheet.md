---
title: "Subscription Manager Cheatsheet"
date: 2020-12-06T12:18:17+01:00
summary: |
    I always forgot how to use subscription-manager, so I wrote my own small cheatsheet.
---

This is my tiny personal cheatsheet for `subscription-manager` that I wrote because I always forget how to use it. If you need something more comprehensive, see [this one](https://access.redhat.com/sites/default/files/attachments/rh_sm_command_cheatsheet_1214_jcs_print.pdf) on the Red Hat Customer Portal.

To be able to access the RHEL CDN repositories you need to firstly register the system, and then attach it to a valid subscription. Let's dive deep in the first step.

## Registering the system
Registering is somewhat like authentication: When you register your system, Red Hat knows who runs the machine. However, bear in mind that the machine is still not authorized to access the CDN repositories.

If you want to register the system interactively, just use the following command:

```bash
sudo subscription-manager register
```

If you want to register your system in a script, you can use one of the following methods:

```bash
# if you have a username and password:
subscription-manager register --username=USERNAME --password=PASSWORD

# if you have an activation key:
subscription-manager register --activationkey=ACTIVATION_KEY --org=ORG_NUMBER
```

## Attaching a subscription to the system
When your system is registered, you need to attach it to a subscription. This is somewhat like authorization: Now, we're authenticated and we need to authorize the system so it can consume the RHEL CDN repositories. There are two important facts about subscriptions:

1) You can have multiple active subscriptions on your account.
2) Different subscriptions authorize you to use a different set of repositories. In other words, some repositories might not be available when using some subscriptions.

Bear those two facts in mind and choose wisely which subscription to use on your system.

### Let subscription-manager decide which subscription to use
`subscription-manager` is pretty smart so it can decide which subscription is the most appropriate for your system. If you use this option, be sure to check the subscription afterwards, `subscription-manager` might choose it wrongly in some situations.

If you use a username and a password to register the system, you can auto attach the subscription in one command:

```bash
subscription-manager register --username=USERNAME --password=PASSWORD --auto-attach
```

If you use an activation key, you have to use a separate `attach` command:

```bash
subscription-manager attach --auto
```

### Choose the subscription manually
Sometimes, it might be more appropriate to choose a specific subscription manually. Firstly, use `subscription-manager` to list available ones:

```bash
subscription-manager list --available
```

When you are decided which subscription you want to use, simple use the following command to attach it:

```bash
subscription-manager attach --pool=POOL_ID
```

After the command has finished, you can see it attached using:

```bash
subscription-manager list --consumed
```

## Enabling additional repositories
The following command can be used to show all repositories that are currently enabled on your system:
```bash
subscription-manager repos --list-enabled
```

If you want to enable an additional one, you can use this command to to see all repositories that you can access with your attached subscription:
```bash
subscription-manager repos
```

To enable a specific repository, just use:
```bash
subscription-manager repos --enable=REPO_ID
```

I, for example, need quite often the codeready builder repository, so I can just use:

```bash
subscription-manager repos --enable=codeready-builder-for-rhel-8-x86_64-rpms
```

To disable a repository, the `--disable` flag is your friend:
```bash
subscription-manager repos --disable=REPO_ID
```

## Removing the subscription and unregistering the system:
If you need to revert the steps from previous paragraphs, you can use the following commands:

```bash
subscription-manager remove --pool=POOL_ID

subscription-manager unregister
```

In some cases, it might be handy to remove all subscriptions at once:

```bash
subscription-manager remove --all
```
