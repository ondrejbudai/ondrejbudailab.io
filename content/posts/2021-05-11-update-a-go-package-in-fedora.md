---
title: "Updating a Go package in Fedora"
date: 2021-05-11T10:35:24+02:00
summary: |
    A quick guide about putting up-to-date Go code into an RPM.
---

Just a quick-n-dirty post to remind me the process needed to update a Go package in Fedora.

1) Check the changelog

Backward-incompatible updates should go only to Rawhide. If there are any breaking changes, check the dependencies with the following command and decide if the changes can be handled in a safely manner:

```bash
dnf repoquery '--disablerepo=*' '--enablerepo=*-source' --arch=src --whatrequires 'golang(github.com/getkin/kin-openapi/openapi3)'
```

> This command prints all packages that BuildRequires the package. This is usually the most important thing to check for Go packages.

2) Run go2rpm in dist-git

```bash
go2rpm https://github.com/labstack/echo
```

3) Stage the relevant changes

```bash
git add -p .
git restore .
```

4) Download the new sources from upstream

```bash
spectool -g golang-github-labstack-echo-4.spec
```

5) Upload the new sources to the Fedora's look aside cache

```bash
fedpkg new-sources echo-4.3.0.tar.gz
```

6) Add a changelog entry to the spec file

```text
* Tue May 11 2021 Ondřej Budai <ondrej@budai.cz> - 4.3.0-1
- Update to 4.3.0
- Close: rhbz#1958599
```

7) Stage and commit the changes

```bash
git add .
git commit
```

The commit message should be something like:

```text
Author: Ondřej Budai <ondrej@budai.cz>
Date:   Tue May 11 10:10:46 2021 +0200

    Update to 4.3.0

    Close: rhbz#1958599
```

8) Make a scratch build

```bash
fedpkg scratch-build --srpm
```

---

From here, things should be simple.
