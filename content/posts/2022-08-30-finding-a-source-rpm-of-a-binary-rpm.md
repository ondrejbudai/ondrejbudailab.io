---
title: "Finding a source RPM of a binary RPM"
date: 2022-08-30T16:46:16+02:00
summary: |
    Finding an SRPM of a binary RPM is simple, yet I wasn't able to find how to do it.
    Therefore, I decided to write about it.
---

You can build several binary RPMs out of one source RPM.
Given a binary RPM, how can you find the name of the source RPM?
This article goes through various methods to find that out.

## Quick overview of source RPMs in Fedora
All sources for packages in Fedora are stored in a so called [dist-git](https://src.fedoraproject.org/).
For example, sources for `cockpit`, the web console, are stored under [rpm/cockpit](https://src.fedoraproject.org/rpms/cockpit).
The `cockpit` source RPM (also referred to as SRPM) is used to build several packages:
- `cockpit` (core),
- `cockpit-networkmanager` (plugin for Network management)
- `cockpit-storaged` (plugin for managing storage)

and much more.

In the case of the `cockpit-networkmanager` package, it's not hard to guess that it probably comes either from `cockpit`, or `cockpit-networkmanager` source packages.

RPM doesn't restrict its users to always use the source package name as a prefix though.
A good example is the [`httpd`](https://src.fedoraproject.org/rpms/httpd) (Apache 2 web server) source package.
Of course, the main binary package built out of it is named `httpd`.
Nevertheless, this SRPM is also used to build some popular modules.
These modules are shipped in binary RPMs named `mod_ssl` or `mod_ldap` etc.

The question is: How to find out that the `mod_ssl` RPM was built from the `httpd` SRPM?

## Using `rpm -qi`
If you have the package installed, you can just run `rpm -qi [PACKAGE_NAME]` to find out, see the following example:

```console
$ rpm -qi mod_ssl
Name        : mod_ssl
...
Signature   : RSA/SHA256, Mon Jun 27 12:54:03 2022, Key ID 999f7cbf38ab71f4
Source RPM  : httpd-2.4.54-3.fc36.src.rpm
Build Date  : Fri Jun 17 13:09:36 2022
...
```

As you can see, the name that you looked for is under the *Source RPM* field.

## Using `dnf info`
If you don't have the package installed, you can always use `dnf` directly to find out, see:

```console
# dnf info mod_ssl
Failed to set locale, defaulting to C.UTF-8
Updating Subscription Management repositories.
Last metadata expiration check: 2:21:28 ago on Tue Aug 30 17:00:05 2022.
Available Packages
Name         : mod_ssl
...
Size         : 111 k
Source       : httpd-2.4.54-3.fc36.src.rpm
Repository   : updates
...
```

The name is shown under the *Source* field.

## Downloading the source RPM
Just a quick bonus to end this article.
If you want to inspect the source RPM yourself, you can download it using `dnf download --source`:
```console
# dnf download --source mod_ssl
...
httpd-2.4.54-3.fc36.src.rpm
# file httpd-2.4.54-3.fc36.src.rpm
httpd-2.4.54-3.fc36.src.rpm: RPM v3.0 src
```

## TL;DR
Use `rpm -qi <NAME>` for installed packages, `dnf info <NAME>` doesn't care if the package is installed.
`dnf download --source` can be used to download the source package.
