---
title: "IPv6 With Network Manager on Hetzner Cloud"
date: 2023-01-24T11:50:35+01:00
summary: |
    Custom images on Hetzner Cloud don't have IPv6 configured correctly by default.
    This short post explains how to do the configuration using Network Manager's `nmcli` on Fedora CoreOS.
---

Major Hayden has [a wonderful blogpost](https://major.io/2021/08/20/deploy-fedora-coreos-in-hetzner-cloud/) about using [Fedora CoreOS](https://getfedora.org/en/coreos) at my favourite cloud service provider, [Hetzner Cloud](https://www.hetzner.com/cloud). However, a machine deployed this way doesn't have a working IPv6 connection. Since Fedora CoreOS ships with Network Manager, it's easy to fix this with just a few `nmcli` commands.

First, you need to find out which subnet you have been assigned. If you are using the `hcloud` CLI, just run:

```bash
hcloud server ip -6 <YOUR SERVER NAME>
# Prints the IP address: 2a01:4f8:c17:a049::1
```

The command actually gives you the first address of the subnet, which has a prefix of `/64`. So the subnet assigned to your instance is `2a01:4f8:c17:a049::/64`. If you are not using the CLI client, you can find the subnet in the instance's overview.

Now, you need to decide which address from the subnet you actually want to use for the server. Conveniently, you can use the first one (which is actually the output from the `hcloud` command above), so we will use `2a01:4f8:c17:a049::1` for this example.

It's time to change the network configuration on the server! Run the following commands as root. I assume that your connection is called `Wired connection 1` which is the default name for Fedora CoreOS.

```bash
nmcli connection modify 'Wired connection 1' ipv6.addresses <The address from the previous step>
# For this example, it's 2a01:4f8:c17:a049::1
nmcli connection modify 'Wired connection 1' ipv6.method manual
nmcli connection modify 'Wired connection 1' ipv6.gateway fe80::1
nmcli connection modify 'Wired connection 1' ipv6.dns 2a01:4ff:ff00::add:1,2a01:4ff:ff00::add:2
nmcli connection up 'Wired connection 1'
```

Let's take a closer look at these commands:

- The first one adds the IP address from the subnet that Hetzner gives us
-  `ipv6.method manual` disables any attempts by Network Manager to autoconfigure the global address (using RA or DHCPv6)
- `ipv6.gateway fe80::1` sets the address of the default gateway, this value is taken from [Hetzer's docs](https://docs.hetzner.com/cloud/servers/primary-ips/primary-ip-configuration/)
- `ipv6.dns` configures the DNS servers for IPv6, again taken from [the docs](https://docs.hetzner.com/dns-console/dns/general/recursive-name-servers/)
- Finally, the last command reloads the configuration.

From now on, your server should be fully IPv6 enabled! You can now try to access IPv6-only websites from the server:

```bash
curl -6 ipv6.google.com
```

and connect to the server's SSH server from another location:

```bash
ssh <YOUR USER>@2a01:4f8:c17:a049::1
```

And that's it! Note that I'm still an IPv6 beginner, so if you find any mistakes, ping me at [@ondrejbudai@fosstodon.org](https://fosstodon.org/@ondrejbudai).
