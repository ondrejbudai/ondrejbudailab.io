#!/bin/bash

if [ $# -ne 1 ]; then
    echo 'Usage: ./new-post.sh "New and awesome post"'
    exit 1
fi

hugo new "posts/$(date +%Y-%m-%d)-$1.md"
