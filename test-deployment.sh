#!/bin/bash

if [[ $# != 2 ]]; then
    echo usage: $0 SHA URL
    exit 1
fi

SHA="$1"
URL="$2"

for i in {1..300}; do
    if curl -sL "$URL" | grep "$SHA">/dev/null; then
        echo Ready!
        exit 0
    fi
    echo Not ready, waiting!
    sleep 1s
done

echo Timeout, exit 1

exit 1
